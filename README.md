# Gitlab CI Templates Docker Example

Here is an example of a project that builds Docker images and pushes them to the container registry
provided by GitLab, using my CI templates for Docker.

[Merge request #1](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/-/merge_requests/1) pushes the image using the project's name after it was merged, as [gitlab-ci-templates-docker-example](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/container_registry/5797725). It also has a tag called "1", which was created by this [pipeline](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/-/pipelines/1077704665) after I created this [tag](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/-/tags/1).

[Merge request #2](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/-/merge_requests/1) pushes the same image to a subdirectory, as [gitlab-ci-templates-docker-example/my_project](https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example/container_registry/5797728).
